'use strict';

angular
  .module('services.login', [])
  .service('loginService', loginService);

loginService.$inject = ['$http', '$rootScope'];

function loginService($http, $rootsScope) {
  var vm = this;

  this.login = login;
  this.clearAuthentication = clearAuthentication;

  $rootsScope.globals = {
    currentUser: {}
  };

  function login(username, password) {
    var method = 'post';
    var headers = {
      'Access-Control-Allow-Origin': '*'
    };
    var data = {name: username, password: password};
    var url = 'http://localhost:3000/api/authenticate';
    clearAuthentication();

    return $http.post(url, {data:data, headers:headers});
  }

  function setAuthentication(username, token) {
      $rootsScope.globals.currentUser = {
        username: username,
        token: token
      };
  }

  function clearAuthentication() {
    $rootsScope.globals.currentUser = {};
  } 

  function successAuthentication(username, token) {
    vm.setAuthentication(username, token);
  }
}