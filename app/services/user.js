'use strict';

angular
  .module('services.users', [])
  .service('userService', userService);

userService.$inject = ['$http'];

function userService($http) {
  this.getAll = getAll;

  function getAll() {
    return $http.get('localhost:3000/api/users');
  }
}