'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'controllers.login',
  'controllers.list',
  'services.login',
  'services.users',
])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $routeProvider.otherwise({redirectTo: '/login'});
}])
.run(['$http', function($http) {
  $http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
  $http.defaults.headers.post['dataType'] = 'json';
}]);
