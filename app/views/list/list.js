'use strict';

angular.module('controllers.list', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/list', {
    templateUrl: 'views/list/list.html',
    controller: 'listCtrl'
  });
}])

.controller('listCtrl', ['$scope', function($scope) {
  $scope.users = [
    {name: 'Lucas'},
    {name: 'Claudio'},
    {name: 'Carlos'},
    {name: 'Diogo'},
    {name: 'Leo'},
  ];
}]);