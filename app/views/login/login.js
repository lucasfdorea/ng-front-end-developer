'use strict';

angular.module(
  'controllers.login',
  [
    'ngRoute', 
    'services.login'
  ]
)

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'views/login/login.html',
    controller: 'loginCtrl'
  });
}])

.controller('loginCtrl', ['$scope', 'loginService', function($scope, loginService) {
  (function initController() {
    loginService.clearAuthentication();
  });

  $scope.login = login;

  function login() {
    var username = $scope.username;
    var password = $scope.password;
    loginService.login(username, password);
  }
}]);;